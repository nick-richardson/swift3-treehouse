// String Literals
let country = "United States of America"
let state = "Indiana"
let city = "Indianapolis"
let street = "West Street"
let buildingNumber = 222

// String Concatentation
let address = country + ", " + state + ", " + city
//let streetAddress = buildingNumber + street | Does not complie due to string value and number value

// String Interpolation
let interpolatedAddress = "\(country), \(state), \(city)"
let interpolatedStreetAddress = "\(buildingNumber) \(street)"

/*
 -----------------------
 Integers
 -----------------------
 */

let favoriteProgrammingLanguage = "Swift"
let year = 2014 // Int

/*
 -----------------------
 Floating Point Numbers
 -----------------------
 */

var version = 3.0 // Double

/*
 -----------------------
 Boolean
 -----------------------
 */

let isAwesome = true // Boolean