// Variables

var str = "Hello, playground"
str = "Hello, world!"

var number = 10
number = 20

// Constants
let language = "Swift"

// Naming Conventions

// Rule #1: Spaces not allowed in varibale names
// let programming Language = "Objective-C" | Wrong!
// let programmingLanguage = "Objective-C"  | Right!

// Rule #2: Use camle case
// let programminglanguage = "Objective-C"  | Wrong!
// let programmingLanguage = "Objective-C"  | Right!

